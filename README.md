# DataScience project:
### By Koffi & Igor
- https://www.amazon.com/b?ie=UTF8&node=8296344011
- https://intellij-support.jetbrains.com/hc/en-us/community/posts/207674845-Adding-a-python-interpreter-to-Intellij-not-pycharm-
- https://www.youtube.com/watch?v=r40pC9kyoj0
- https://www.youtube.com/watch?v=mkKDI6y2kyE
```
ila@ila:~/vhosts/datascience(master)$ FLASK_APP=hello.py flask run
 * Serving Flask app "hello.py"
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
127.0.0.1 - - [06/Nov/2018 20:15:44] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [06/Nov/2018 20:15:44] "GET /favicon.ico HTTP/1.1" 404 -
127.0.0.1 - - [06/Nov/2018 20:25:33] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [06/Nov/2018 20:25:34] "GET / HTTP/1.1" 200 -

```


### Flask in 0.0.0.0:80 EC2 - in AWS
- https://stackoverflow.com/questions/20212894/how-do-i-get-flask-to-run-on-port-80
- https://pt.stackoverflow.com/questions/346328/como-subir-um-servidor-flask-em-0-0-0-080-num-linux-ubuntu-16-04-com-python3-nu

```
//app.py
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"
    
    
sudo FLASK_APP=app.py flask run --host=0.0.0.0 --port=80

```





- https://github.com/github/gitignore/blob/master/Global/JetBrains.gitignore
- https://www.atlassian.com/git/tutorials/saving-changes/gitignore
- https://medium.com/google-cloud/how-we-implemented-a-fully-serverless-recommender-system-using-gcp-9c9fbbdc46cc


### Install Flask in EC2 Ubuntu 16.04
- Confirming that python2 is not installed
``
$ python2
  The program 'python2' is currently not installed. You can install it by typing:
  sudo apt install python-minimal
``

- Confirming that python3 is installed

```
$ python3
Python 3.5.2 (default, Nov 12 2018, 13:43:14) 
[GCC 5.4.0 20160609] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
KeyboardInterrupt

```


- Confirming that pip3 is not installed
```
 pip3 --version
 The program 'pip3' is currently not installed. You can install it by typing:

```
- Installing pip3 AND Flask
```
 sudo apt-get update
 sudo apt-get -y install python3-pip
 sudo pip3 install flask
```


http://docs.h2o.ai/h2o-tutorials/latest-stable/tutorials/hive_udf_template/hive_udf_pojo_template/index.html

https://www.youtube.com/watch?v=t24ZOk06wa8